from flask import Flask, url_for, render_template
app = Flask(__name__)
import sys
from xml.dom.minidom import parse
import xml.dom.minidom
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import operator
from operator import itemgetter
import psycopg2
reload(sys)  
sys.setdefaultencoding('UTF')

@app.route('/')
def index():
	return 'welcome to home page'
@app.route('/check-db')
def connect_db():
	conn_string = "host='localhost' dbname='db_python' user='aadil' password='cogilent123'"
	conn = psycopg2.connect(conn_string)
	cursor = conn.cursor()
	#ddata= cursor.execute("create table table1(name char(40));")
	rty = cursor.execute("insert into table1 values('testing second try')")
	conn.commit()
	return conn,cursor
@app.route('/make-colleges-list')
def read_file():
	with open('polytechnique_institues_list.txt') as f:
		content = f.read()
		con,cur = connect_db()
    	cur.execute("create table tb_polytechnique_institutes_list (id int , name char(400))")
    	#cur.execute("delete from tb_colleges_list")
    	lines = content.split('\n')
    	#cur.execute("select MAX(id) from tb_colleges_list")
    	#max_val = cur.fetchall()
    	#counter = max_val[0][0]
    	#counter = 0 remove comment only when all data from table is deleted 
    	counter = 0
    	for line in lines:
    		if line:
    			counter = counter + 1
    			line = line.strip()
    			que = "insert into tb_polytechnique_institutes_list VALUES ('%d','%s')" % (counter, line)
    			cur.execute(que)
    	con.commit()
    	print "institutes added :"+str(counter)
    	return "Polytechnique list cereated "
@app.route('/view-data')
def use_raw_data():
	
	conn,cur = connect_db()
	cur.execute("select count(id) from tb_filtered_list")
	qwe = cur.fetchall()
	print qwe[0][0]
	cur.execute("select * from tb_colleges_list")
	#cur.execute("select * from tb_raw_data")
	da = cur.fetchall()
	conn.commit()
	return render_template('display-text.html', data= da)
	
if __name__ == '__main__':
    app.run(host='0.0.0.0')
