from flask import Flask, url_for, render_template
app = Flask(__name__)
import sys
from xml.dom.minidom import parse
import xml.dom.minidom
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import operator
from operator import itemgetter
import psycopg2
reload(sys)  
sys.setdefaultencoding('UTF')

@app.route('/')
def index():
	return 'welcome to home page'
@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
	return render_template('hello.html', name=name)
@app.route('/user/<username>')
def show_user_profile(username):
	return 'User %s' % username
@app.route('/post/<int:post_id>')
def show_post(post_id):
	return 'Post %d' % post_id
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        do_the_login()
    else:
        show_the_login_form()
def show_the_login_form():
	return render_template('login.html',post_on='/verify_login')
def ordinary_word(word):
	or_words = []
	or_words.append('the')
	or_words.append('of')
	#or_words.append('the')
	if not word in or_words:
		return 1
	else:
		return 0
def already_added(lower_case):
	if lower_case in saved_names:
		return 1
	else:
		return 2
def convert_to_string(words_array):
	tmp_line = ''
	if len(words_array)>0:
		for word in words_array:
			tmp_line = tmp_line +' '+ word
	return tmp_line
def make_abbreviation(lower_case):
	#return 'working on it'
	abb = ''
	for word in lower_case:
		if ordinary_word(word) == 1:
			abb = abb + word[:1]
	return abb
def exists(sou,des):
		qwerty_count = 0
		t_matching = 0
		t_name = ''
		seq_num = 0
		t_rep = 0
		for lll in des:
			if lll and des[lll]['1'] == sou :
				#t_matching = fuzz.token_set_ratio( des[lll]['1'] , sou )
				t_rep = des[lll]['4']
				t_name = des[lll]['1']
				seq_num = qwerty_count
				return des[lll]['4'], qwerty_count, 'none'
			qwerty_count= qwerty_count + 1	
		return -1,-1 , ''
def split_in_columns(line):
	return line.split('"')
	tt_array = []
	items = 1
	if line:
		count3 = -1
		count = 0
			#for ch in line:
			#logic for old file , with double cots along all parameters
			# if ch=='"' and count > count3:
			# 	start = count + 1
			# 	count2 = 0
			# 	#count = count + 1
			# 	for ch2 in line:
			# 		if count2 > count:
			# 		 	if ch2 == '"':
			# 		 		end = count2 - 1
			# 		 		if not start > end:
			# 		 			tt_array.append(line[start:end+1])
			# 		 	 	else:
			# 		 	 		tt_array.append('')
			# 		 	 	items = items + 1
			# 		 	 	count3 = count2
			# 		 	 	break
			# 		count2 = count2 + 1
			# count = count + 1

	return tt_array
def make_abbreviation_forstring(txt):
	str_arr = txt.replace('and','')
	str_arr = str_arr.replace('&','')
	str_arr = str_arr.replace('of','')
	str_arr = str_arr.replace('the','')
	str_arr = str_arr.replace(',','')
	str_arr = str_arr.split(' ')
	abb_tmp = ''
	for word in str_arr:
		abb_tmp = abb_tmp+word[:1]
	return abb_tmp
def make_cities_list(con,cur):
	cur.execute("select * from tb_cities_list")
	cities = []
	lines = cur.fetchall()
	for city_f in lines:
		city  = city_f[1]
		txt_t = city.lstrip()
		txt_t = txt_t.lower()
		cities.append(txt_t)
	return cities	
@app.route('/make-master-list')
def make_master_list(con,cur):
	dummy = {}
	cur.execute("select * from tb_master_list")
	lines = cur.fetchall()
	counter = 0
	limit = 3000
	for line in lines:
		abb = ''
		counter= counter + 1
		institute = line[1]
		institute = institute.replace("&nbsp;","")
		abb = make_abbreviation_forstring(institute)
		dummy[institute]={'name':institute,'abb':abb}	        				
	return dummy
@app.route('/check-db')
def connect_db():
	conn_string = "host='localhost' dbname='db_python' user='aadil' password='cogilent123'"
	conn = psycopg2.connect(conn_string)
	cursor = conn.cursor()
	#ddata= cursor.execute("create table table1(name char(40));")
	rty = cursor.execute("insert into table1 values('testing second try')")
	conn.commit()
	return conn,cursor
def is_not_college_or_school(degree,list_arr):
	degree = degree.replace('.','')
	degree = degree.replace(' ','')
	degree = degree.lower()
	for val in list_arr:
		if val in degree:
			return False
	return True
@app.route('/read-data')
def read_data(con,cur):
		cur.execute("select * from tb_pointer where name = 'position'")
		qwe = cur.fetchall()
		start=  qwe[0][1]
		increment = 5000
		end = start + increment
		q1 = "SELECT  * FROM tb_raw_data WHERE id >= '%d' AND id < '%d' " %(start,end)
		q2 = "UPDATE tb_pointer SET location = '%d' WHERE name='position'" % (increment+start)
		print q1
		#print q2
		cur.execute(q2) 
		cur.execute(q1) 
		con.commit()
		lines = cur.fetchall()
		d_array = []
		saved_names = []
		dictionary = {}
		#counter = 0
		col_schl = ["matric","metric","ics","fsc","hssc","ssc","dae","matriculation",]
		for line in lines:
			#counter = counter + 1
			# if counter > 3000:
			#  	break
			if is_not_college_or_school(line[1],col_schl):
				if line[2]:
					dat = line[2]
					#print counter
	    			if len(dat) > 1 :
	    				lower_case = line[2].lower()
	    				lower_case = lower_case.replace(',','')
	    				_original_line = lower_case #added original line
	    				abb = make_abbreviation_forstring(lower_case)
	    				_abbreviation = abb #abbreviation added
	    				#print _abbreviation
	    				_repitition = 1
	    				(noc,dd,to_del) = exists(_original_line,dictionary)
	    				if noc == -1:
	    					_repitition = 1
	    					#print "No repeat"
	    				else:
	    					
	    					_repitition = noc + 1
	    					#print "repeated ="+ str(_repitition)
	    					#print _original_line + str(noc)
	    					del dictionary[_original_line]
	    				dictionary[_original_line] = {'1':_original_line,'2':'none','3':_abbreviation,'4':_repitition}
	    	#else:

   		#print "reading done"
		return dictionary
def contains_city(tmp_name,cities_list):
	for city in cities_list:
		if city and city in tmp_name:
			return 0
	return 1
def hec_confidential_score(obj2,hec_list):
	tmp_name = ''
	tmp_matching = 0
	abb_name = ''
	abb_matching = 0
	match_for_abb = obj2
	for ooo in hec_list:
		#print obj2
		ooo_tmp = ooo.replace('&nbsp;',' ')
		ooo_tmp = ooo.replace('&','')
		ooo_tmp = ooo_tmp.replace('and','')
		#obj2_abb = make_abbreviation(obj2)
		if fuzz.token_set_ratio(obj2, ooo_tmp) > tmp_matching:
			tmp_matching = fuzz.token_sort_ratio(obj2, ooo.replace('&nbsp;',' '))
			tmp_name = obj2
		r_t = hec_list[ooo]['abb']
		if fuzz.ratio(match_for_abb,r_t) > abb_matching:
			abb_matching = fuzz.token_set_ratio(match_for_abb,r_t)
			abb_name = hec_list[ooo]['name']
	if tmp_matching >60:
		return tmp_matching/2
	else :
		if abb_matching > 80:
			return abb_matching/2
		else :
			return tmp_matching/2
def data_confidential_score(obj3):
	if(obj3 > 50 ):
		return 50
	else:
		return obj3
def find_similar_row_in_db(ins_name,cur):
	que = "select * from tb_filtered_list where name = '%s'" % (ins_name)
	#if "peshawar" in ins_name:
	#	print que
	#print ins_name
	cur.execute(que)
	tmp_data = cur.fetchall()
	inst_occ = 0
	inst_id =0
	chk = 0
	for ww in tmp_data:
		chk = chk + 1
		inst_occ = ww[3]
		inst_id = ww[0]
		#print ins_name+ ":" + str(inst_occ)
		#print ww[1]
		return inst_id,inst_occ
	return -1,-1
		
def write_result_to_db(fin_dict,con,cur):
	print "Length of data uploaded"+str(len(fin_dict))
	cur.execute("delete from tb_filtered_list")
	#cur.execute("create table tb_filtered_list(id int , name char(400), hec_occ int, data_occ int, category char(100), PRIMARY KEY (id));")
	cur.execute("SELECT MAX(id) As hv FROM tb_filtered_list;")
	max_v = cur.fetchall()
	counter = 0
	if max_v[0][0]:
		counter = max_v[0][0]
	for data in fin_dict:
		counter = counter + 1
		#print fin_dict[data]['name']+":"+str(fin_dict[data]['hec-occurence'])+":"+str(fin_dict[data]['data-occurence'])+":"+fin_dict[data]['category']
		id_of_row,row_data_occ = find_similar_row_in_db(fin_dict[data]['name'],cur)
		que = ''
		if id_of_row == -1:
			#print "new rec added"
			que = "INSERT INTO tb_filtered_list VALUES  ('%d','%s', '%d', '%d', '%s' )" % (counter,fin_dict[data]['name'], fin_dict[data]['hec-occurence'], fin_dict[data]['data-occurence'], fin_dict[data]['category'])
			cur.execute(que)
			#if "peshawar" in fin_dict[data]['name']:
			#	print que
		else:
			#print "wrong conditino called"+fin_dict[data]['name']
			fin_data_occ = row_data_occ + fin_dict[data]['data-occurence']
			#print str(row_data_occ) +":"+str(fin_dict[data]['data-occurence'])
			#fin_data_occ = fin_data_occ % 50
			if fin_data_occ > 50:
				#print "max limit achieved"
				#print fin_dict[data]['name'] + ":"+cur + ":" +str(fin_data_occ)
				fin_data_occ = 50
			#print fin_dict[data]['data-occurence']
			#print row_data_occ
			#fin_data_occ = fin_data_occ % 50
			#print fin_dict[data]['name']
			ffff= fin_dict[data]['data-occurence'] + row_data_occ
			#print ffff
			#print id_of_row
			#fin_data_occ = fin_data_occ % 50
			cat= 'unknown institution2'
			if ffff > 70:
				cat = "university"

			que = "UPDATE tb_filtered_list SET data_occ = '%d', category = '%s' WHERE id='%d'" % (fin_data_occ,ffff,id_of_row) 
			#print que
			cur.execute(que)
			#print que
			#print "update query successfull"
	con.commit()
	print "finalized data added in db"	
@app.route('/make-cofidential-score')
def make_confidential_score():
	con,cur = connect_db()
	fin_dict = {}
	count = 0
	print "creating hec list"
	hec_list = make_master_list(con,cur) #list of hec data 
	print "hec list created"
	print "creating raw-data read list"
	file_data = read_data(con,cur)
	print "raw data read"
	print "calculating cofidence score"
	for obj in file_data:
			#count = count + 1
			#print count
		 	obj2_tmp2 = obj.replace('&','')
		 	obj2_tmp2 = obj2_tmp2.replace('and','')
		 	#print "caclutaing hec confindential score"
		   	hec_conf = hec_confidential_score(obj2_tmp2,hec_list) # confidential score calculated from hec master list 
		   	#print "caclutaing data confindential score"
		   	occ_conf = data_confidential_score(file_data[obj]['4']) #cs from file data
		   	obj2_tmp2 = obj2_tmp2.replace('^','')
		   	ins_type = ''
		   	sum_conf_scr = 0
		   	sum_conf_scr = hec_conf + occ_conf
		   	if sum_conf_scr  > 70: 
		   		ins_type = "University"
		   	else :
		   		#print obj2_tmp2+str( hec_conf+occ_conf)
		   		ins_type = "Unkown institution1"
		   	fin_dict[obj2_tmp2] = {'name':obj2_tmp2,'hec-occurence':hec_conf, 'data-occurence':occ_conf, 'category':ins_type}
	print "confidence score calculated"	   	
	write_result_to_db(fin_dict,con,cur)
	return "data uploaded, successfull"
@app.route('/save-master-list-in-db')
def add_master_list_to_db():
	conn,cur = connect_db()
	count = 0
	raw_dict = read_data(con,cur)
	#cur.execute("select * from tb_raw_data")
	#for xx in master_list:
	#	count = count +1
	#	#que = "insert into tb_master_list values ("+str(count)+",'"+master_list[xx]['name']+"','"+master_list[xx]['abb']+"')"
	#	que = "INSERT INTO tb_cities_list VALUES ('%d', '%s' )" % (count, xx)
	#	cur.execute(que)
	#conn.commit()
	#result = cur.fetchall()
	#return 'data added'
	#for line in raw_dict :
		#que = ""
		#cur.execute(que)
	return render_template('display-text.html',data = result)
@app.route('/view-data')
def use_raw_data():
	#		for adding raw data to db
			# for line in file_data:
			# 	count = count + 1
			# 	org_line = file_data[line]['1']
			# 	sor_line = file_data[line]['2']
			# 	abb = file_data[line]['3']
			# 	occ = file_data[line]['4']
			# 	if abb:
			# 		query = "INSERT INTO tb_data_read VALUES ('%d', '%s', '%s', '%s', '%d' )" % (count, org_line, sor_line, abb, occ)
			# 		cur.execute(query)
			# conn.commit()
	conn,cur = connect_db()
	cur.execute("select sum(data_occ) from tb_filtered_list")
	qwe = cur.fetchall()
	print qwe[0][0]
	cur.execute("select * from tb_filtered_list ORDER BY data_occ DESC, hec_occ DESC")
	#cur.execute("select * from tb_raw_data")
	da = cur.fetchall()
	conn.commit()
	return render_template('display-text.html', data= da)
	
if __name__ == '__main__':
    app.run(host='0.0.0.0')
