from flask import Flask, url_for, render_template
app = Flask(__name__)
import sys
from xml.dom.minidom import parse
import xml.dom.minidom
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import operator
from operator import itemgetter
import psycopg2
reload(sys)  
sys.setdefaultencoding('UTF')

@app.route('/')
def index():
	return 'welcome to home page'
def ordinary_word(word):
	or_words = []
	or_words.append('the')
	or_words.append('of')
	#or_words.append('the')
	if not word in or_words:
		return 1
	else:
		return 0
def already_added(lower_case):
	if lower_case in saved_names:
		return 1
	else:
		return 2
def convert_to_string(words_array):
	tmp_line = ''
	if len(words_array)>0:
		for word in words_array:
			tmp_line = tmp_line +' '+ word
	return tmp_line
def make_abbreviation(lower_case):
	#return 'working on it'
	abb = ''
	for word in lower_case:
		if ordinary_word(word) == 1:
			abb = abb + word[:1]
	return abb
def exists(sou,des):
		qwerty_count = 0
		t_matching = 0
		t_name = ''
		seq_num = 0
		t_rep = 0
		for lll in des:
			if lll and des[lll]['1'] == sou :
				#t_matching = fuzz.token_set_ratio( des[lll]['1'] , sou )
				t_rep = des[lll]['4']
				t_name = des[lll]['1']
				seq_num = qwerty_count
				return des[lll]['4'], qwerty_count, 'none'
			qwerty_count= qwerty_count + 1	
		return -1,-1 , ''
def split_in_columns(line):
	return line.split('"')
	tt_array = []
	items = 1
	if line:
		count3 = -1
		count = 0
			#for ch in line:
			#logic for old file , with double cots along all parameters
			# if ch=='"' and count > count3:
			# 	start = count + 1
			# 	count2 = 0
			# 	#count = count + 1
			# 	for ch2 in line:
			# 		if count2 > count:
			# 		 	if ch2 == '"':
			# 		 		end = count2 - 1
			# 		 		if not start > end:
			# 		 			tt_array.append(line[start:end+1])
			# 		 	 	else:
			# 		 	 		tt_array.append('')
			# 		 	 	items = items + 1
			# 		 	 	count3 = count2
			# 		 	 	break
			# 		count2 = count2 + 1
			# count = count + 1

	return tt_array
def make_abbreviation_forstring(txt):
	str_arr = txt.replace('and','')
	str_arr = str_arr.replace('&','')
	str_arr = str_arr.replace('of','')
	str_arr = str_arr.replace('the','')
	str_arr = str_arr.replace(',','')
	str_arr = str_arr.replace("'","")
	str_arr = str_arr.split(' ')
	abb_tmp = ''
	for word in str_arr:
		abb_tmp = abb_tmp+word[:1]

	return abb_tmp
def make_cities_list(con,cur):
	cur.execute("select * from tb_cities_master_list")
	cities = []
	lines = cur.fetchall()
	for city_f in lines:
		city  = city_f[1]
		txt_t = city.lstrip()
		txt_t = txt_t.lower()
		cities.append(txt_t)
	return cities	
@app.route('/make-master-list')
def make_master_list(con,cur,tbl_name):
	dummy = {}
	query = "select id,name,abb from %s" % (tbl_name)
	cur.execute(query) # tb_polytechnique_institutes_list
	lines = cur.fetchall()
	counter = 0
	for line in lines:
		abb = line[2]
		institute = line[1]
		institute = institute.replace("&nbsp;"," ")
		institute = institute.replace("&amp;"," ")
		institute = institute.replace(",","")
		institute = institute.replace("_"," ")
		institute = institute.replace(".","")

		institute = institute.lower()
		abb = make_abbreviation_forstring(institute)
		dummy[institute]={'id':line[0],'name':institute,'abb':abb}	        				
	return dummy
def remove_if_dual_city(tmp_city,city):
	#print tmp_city
	tmp_index = tmp_city.find(city)
	if tmp_index > -1:
		v_val_1 = tmp_city[0:tmp_index+len(city)]
		v_val_2 = tmp_city[tmp_index+len(city):len(tmp_city)]
		v_val_2 = v_val_2.replace(v_val_2,'')
		tmp_city = v_val_1+v_val_2
	#print tmp_city
	return tmp_city
	#t1[12:len(t1)]
def remove_city_names(_original_line,cities_list):
	#print len(cities_list)
	tmp_city = _original_line
	for city in cities_list:
		city = city.strip()
		if len(city) > 2:
			conc_of = "of "+city
			conc_of_2 = "the "+city
			if conc_of in _original_line or conc_of_2 in _original_line:
				co = 1 +1
				tmp_city = remove_if_dual_city(tmp_city,city)
			else :
				#print city +" <in>" + _original_line
				if city in _original_line:
					#print "city matched"
					tmp_city = tmp_city.replace(city,'')
					#print tmp_city +">"+ _original_line+">"+city
					#print city
				
	#print tmp_city

	return tmp_city
@app.route('/check-db')
def connect_db():
	conn_string = "host='ec2-54-225-120-137.compute-1.amazonaws.com' dbname='d50o3fq3c5v493' user='ofpptufxpgbgor' password='jUeHlLmegfrgKVDns9v5UNEc-T'"
	conn = psycopg2.connect(conn_string)
	cursor = conn.cursor()
	#ddata= cursor.execute("create table table1(name char(40));")
	#rty = cursor.execute("insert into table1 values('testing second try')")
	#conn.commit()
	return conn,cursor
def is_not_college_or_school(degree,list_arr):
	#return True
	degree = degree.replace('.','')
	degree = degree.replace(' ','')
	degree = degree.lower()
	for val in list_arr:
		if val in degree:
			return False
	return True
@app.route('/read-data')
def read_data(con,cur):
		cur.execute("select * from tb_pointer where name = 'position4'")
		qwe = cur.fetchall()
		start=  qwe[0][1]
		#start = 50000	
		#start= 55000 #to remove
		increment = 100
		end = start + increment
		q1 = "SELECT education_id,title,lower(institute),city  FROM education limit %d offset %d " %(increment,start)
		#active belowe line
		q2 = "UPDATE tb_pointer SET location = '%d' WHERE name='position4'" % (end) #dfdf
		print "Reading data from :"+str(start)+" --to-- "+str(end)
		#print q1
		#print q2
		cur.execute(q2) 
		cur.execute(q1) 
		#con.commit()
		lines = cur.fetchall()
		d_array = []
		saved_names = []
		dictionary = {}
		#counter = 0
		counter_index = 0
		for line in lines:
			#counter = counter + 1
			# if counter > 3000:
			#  	break
			if (True):
				if line[1] and len(line[1]) > 1:
					dat = line[1]
					if len(dat) > 1 :
						lower_case = line[1].lower()
						lower_case = lower_case.replace(',','')
	    				lower_case = lower_case.replace('.','')
	    				lower_case = lower_case.replace('(',' ')
	    				lower_case = lower_case.replace(')',' ')
	    				lower_case = lower_case.replace('-',' ')
	    				lower_case = lower_case.decode('ascii','ignore').strip()

	    				_original_line = lower_case #added original line
	    				abb = make_abbreviation_forstring(lower_case)
	    				_abbreviation = abb #abbreviation added
	    				#print _abbreviation
	    				_city = line[3].lower()

	    				
	    				_institute = line[2].replace("'","'")#doing nothing
	    				_institute = _institute.replace(",","")
	    				_institute = _institute.replace("&","")
	    				_institute = _institute.replace("and","")
	    				_institute = _institute.decode('ascii','ignore').strip()
	    				#print _institute

	    				_repitition = 1
	    				#(noc,dd,to_del) = exists(_original_line,dictionary)
	    				#if noc == -1:
	    				_repitition = 1
	    					#print "No repeat"
	    				#else:
	    				counter_index = counter_index + 1	
	    				#	_repitition = noc + 1
	    				#	del dictionary[_original_line]
	    				dictionary[str(counter_index)] = {'1':_original_line,'2':'none','3':_abbreviation,'5':_institute,'4':_repitition,'6':_city}
		return dictionary
def contains_city(tmp_name,cities_list):
	for city in cities_list:
		if city and city in tmp_name:
			return 0
	return 1
def hec_confidential_score(obj2,hec_list):
	tmp_name = ''
	tmp_matching = 0
	abb_name = ''
	abb_matching = 0
	match_for_abb = obj2
	tmp_name_id = -1
	tmp_abb_id = -2
	for ooo in hec_list:
		#print ooo
		sim_1 = fuzz.token_sort_ratio(obj2, ooo)
		if sim_1 > tmp_matching :# and city_city.lower() in ooo_tmp:
			tmp_matching = sim_1
			tmp_name = obj2
			tmp_name_id = hec_list[ooo]['id']
		r_t = hec_list[ooo]['abb']
		sim_2 = fuzz.ratio(match_for_abb,r_t)
		if sim_2 > abb_matching:
			abb_matching = sim_2
			abb_name = hec_list[ooo]['name']
			tmp_abb_id =hec_list[ooo]['id']
	#print obj2+str(tmp_name_id)+"abb"+str(tmp_abb_id)
	if tmp_matching >60:
		return tmp_matching/2,tmp_name_id
	else :
		if abb_matching > 80:
			return abb_matching/2,tmp_abb_id
		else :
			return tmp_matching/2,tmp_name_id
def data_confidential_score(obj3):
	if(obj3 > 50 ):
		return 50
	else:
		return obj3
def find_similar_row_in_db(degree,cur,tbl_name):
	degree = degree.replace("'","''")
	que = "select id,data_occ from %s where name = '%s'" % (tbl_name,degree)
	cur.execute(que)
	tmp_data = cur.fetchall()
	for ww in tmp_data:
		return ww[0],ww[1]
	return -1,-1
@app.route('/get-colleges-from-filtered-list/<search_kind>')
def get_colleges_list_from_filtered_data(search_kind=None):
	con,cur= connect_db()
	val = '%s'%(search_kind)
	val = '%'+val+'%'
	que = ''
	if search_kind == "allcolleges":
		que = "select * from tb_filtered_list WHERE name LIKE '%post graduate%' OR name LIKE '%postgraduate%' OR name LIKE '%degree college%' OR name LIKE '%degreecollege' OR name LIKE '%model college%' OR name LIKE '%modal college%' OR name LIKE '%college of%' AND name NOT LIKE '%university%' ORDER BY (data_occ + hec_occ) DESC"
	else:
		que = "select * from tb_filtered_list WHERE name LIKE '%s' ORDER BY (data_occ + hec_occ) DESC " %(val)
	#print que
	cur.execute(que)

	data = cur.fetchall()
	return render_template('display-text.html',data= data)
def write_result_to_db(fin_dict,con,cur):
	#print "Length of data uploaded"+str(len(fin_dict))
	#cur.execute("delete from tb_filtered_list")
	#cur.execute("create table tb_filtered_list(id int , name char(400), hec_occ int, data_occ int, category char(100), PRIMARY KEY (id));")
	tbl = 'tb_filtered_list'
	query2 = "SELECT MAX(id) As hv FROM %s;" % (tbl)
	cur.execute(query2)
	max_v = cur.fetchall()
	counter = 0
	if max_v[0][0]:
		counter = max_v[0][0]
	for data in fin_dict:
		counter = counter + 1
		#print fin_dict[data]['name']+":"+str(fin_dict[data]['hec-occurence'])+":"+str(fin_dict[data]['data-occurence'])+":"+fin_dict[data]['category']
		id_of_row,row_data_occ = find_similar_row_in_db(fin_dict[data]['name'],cur,tbl)
		que = ''
		if id_of_row == -1:
			#print "new rec added"
			ccc = fin_dict[data]['hec-occurence'] + fin_dict[data]['data-occurence']
			catt= ''
				#if fin_dict[data]['hec-occurence'] > 30 and fin_dict[data]['data-occurence'] >30:
				#	catt = "university"
				#else :
				#	catt= "unknown institution = insert"
			que = "INSERT INTO tb_filtered_list VALUES  ('%d','%s', '%d', '%d', '%s' )" % (counter,fin_dict[data]['name'], fin_dict[data]['hec-occurence'], fin_dict[data]['data-occurence'], fin_dict[data]['category'])
			cur.execute(que)
			#print "1 =:>" + que 
			#if "peshawar" in fin_dict[data]['name']:
			#	print que
		else:
			#print "wrong conditino called"+fin_dict[data]['name']
			fin_data_occ = row_data_occ + fin_dict[data]['data-occurence']
			#print str(row_data_occ) +":"+str(fin_dict[data]['data-occurence'])
			#fin_data_occ = fin_data_occ % 50
			if fin_data_occ > 50:
				#print "max limit achieved"
				#print fin_dict[data]['name'] + ":"+cur + ":" +str(fin_data_occ)
				fin_data_occ = 50
			#print fin_dict[data]['data-occurence']
			#print row_data_occ
			#fin_data_occ = fin_data_occ % 50
			#print fin_dict[data]['name']
			ffff= fin_dict[data]['data-occurence'] + row_data_occ
			#print ffff
			#print id_of_row
			#fin_data_occ = fin_data_occ % 50
			cat= 'unknown institution2'
				#if fin_dict[data]['data-occurence'] > 30 and row_data_occ >30:
				#	cat = "university"
				#else:
				#	cat ="unknown institution =update"
			que = "UPDATE tb_filtered_list SET data_occ = '%d' WHERE id='%d'" % (fin_data_occ,id_of_row) 
			#print "2 =:>" + que 
			cur.execute(que)
			#print que
			#print "update query successfull"
	con.commit()
	print "finalized data added in db"	
def separate_degree_subject(obj):
	deg = ''
	sub = ''
	space_at = obj.find(" ")
	deg = obj[0:space_at]
	sub = obj[space_at+1:len(obj)]
	deg = deg.strip()
	sub = sub.strip()
	deg = deg.replace("'","")
	sub = sub.replace("'","")
	return deg,sub
def make_relations(deg,sub,ins_id,city,cur):
	deg = deg.replace("'","")
	sub= sub.replace("'","")
	que1 = "select id from degrees where name like '%s'" %(deg)
	que2= "select id from subjects where name ='%s'" %(sub)
	cur.execute(que1)
	data1= cur.fetchall()
	cur.execute(que2)
	data2= cur.fetchall()
	if data1 and data2:
		val1=data1[0][0]
		val2=data2[0][0]
		city = city.replace("'","")
		que_run = "insert into specialization (institute_id,degree_id,subject_id,city) values('%d','%d','%d','%s')" %(ins_id,val1,val2,city)
		cur.execute(que_run)	
@app.route('/make-cofidential-score')
def make_confidential_score_degrees():
	con,cur = connect_db()
	fin_dict = {}
	count = 0
	print "Creating Master List"
	degrees_list = make_master_list(con,cur,'tb_degrees_list')#last column is name of table from which master list is to be created
	subjects_list = make_master_list(con,cur,'tb_subjects_list')
	file_data = read_data(con,cur)
	if not file_data:
		return "1"
	print "Now Calculating Confidence Score and writing to db"
	#cities_list = make_cities_list(con,cur) 
	count = 0
	for obj in file_data:
		count = count + 1
		if obj and len(obj)>1:
			val = file_data[obj]['1'].strip()
			deg,sub = separate_degree_subject(val)
			#print deg + ":"+sub
			deg_hec_score,match_with_deg = hec_confidential_score(deg,degrees_list)
			sub_hec_score,match_with_sub = hec_confidential_score(sub,subjects_list)
			city = file_data[obj]['6']
			occ = file_data[obj]['4']
			ins = file_data[obj]['5']
			abb1 = make_abbreviation_forstring(deg)
			abb2 = make_abbreviation_forstring(sub)
			write_degree_to_db(deg,deg_hec_score,abb1,occ,ins,match_with_deg,con,cur,city,"degrees","tb_relation_deg_ins")
			write_degree_to_db(sub,sub_hec_score,abb2,occ,ins,match_with_sub,con,cur,city,"subjects","tb_relation_sub_ins")
			#id_of_ins = get_institute_id(ins,cur)
			#for matching institute
			#lower_case = ins.lower()
			#lower_case = lower_case.replace(",","")
			#lower_case = lower_case.replace("'","''")
			#ins = lower_case
			institute_id = get_institute_id(ins,cur)
			#print str(institute_id) + ":=:" + ins + ":=:"+sub
			print count
			make_relations(deg,sub,institute_id,city,cur)
			#print "Rows added :"+str(count)
	con.commit()
	return 'data added'
@app.route('/add-data-continuously')
def start_loop():
	while True:
		x = make_confidential_score()
		if x == 1:
			break
	return "<h1 style='color:blue'>All data uploaded to Server! Successfull</h1>"
@app.route('/save-master-list-in-db')
def add_master_list_to_db():
	conn,cur = connect_db()
	count = 0
	raw_dict = read_data(con,cur)
	#cur.execute("select * from tb_raw_data")
	#for xx in master_list:
	#	count = count +1
	#	#que = "insert into tb_master_list values ("+str(count)+",'"+master_list[xx]['name']+"','"+master_list[xx]['abb']+"')"
	#	que = "INSERT INTO tb_cities_list VALUES ('%d', '%s' )" % (count, xx)
	#	cur.execute(que)
	#conn.commit()
	#result = cur.fetchall()
	#return 'data added'
	#for line in raw_dict :
		#que = ""
		#cur.execute(que)
	return render_template('display-text.html',data = result)
@app.route('/update-parent')
def up_date():
	con,cur = connect_db()
	mas_list = make_master_list(con,cur,"tb_colleges_master_list")
	cur.execute("select id,name from tb_colleges_score_list where match_with = -1")
	data = cur.fetchall()
	cities_list = make_cities_list(con,cur)
	for val in data:
		name = val[1]
		name = remove_city_names(name,cities_list)
		hec_conf, match_with_id = hec_confidential_score(name,mas_list,"")
		#print name + str(len(mas_list))
		que = "update tb_colleges_score_list set match_with = '%d' where id ='%d'" % (match_with_id,val[0])
		cur.execute(que)
		#print val[1]+":\tmatched with\t:"+str(match_with_id)
	con.commit()
	return "updae done"
@app.route('/view-data')
def view_data():
	conn,cur = connect_db()
	q = "select name,city,(select name from tb_polytechnique_master_list where id = match_with ),mast_occ from tb_polytechnique_score_list order by mast_occ desc;"
	cur.execute(q)
	#print q
	#cur.execute("select * from tb_raw_data")
	da = cur.fetchall()
	#conn.commit()
	return render_template('display-text.html', data= da)
def get_institute_id(ins,cur):
	ins_id = -1
	ins_name = 'none'
	que1 = "select match_with,mast_occ from tb_universities_score_list where mast_occ > 40 and name='%s'" %(ins.replace("'","''"))
	que2 = "select match_with,mast_occ from tb_colleges_score_list where mast_occ > 40 and name='%s'" %(ins.replace("'","''"))
	que3 = "select match_with,mast_occ from tb_polytechnique_score_list where mast_occ > 40 and name='%s'" %(ins.replace("'","''"))
	cur.execute(que1)
	data1 = cur.fetchall()
	cur.execute(que2)
	data2 = cur.fetchall()
	cur.execute(que3)
	data3 = cur.fetchall()
	if data1 and data1[0][1] > 40:
		ins_id = data1[0][0]
		que1_1 = "select name from tb_universities_master_list where id='%d'"%(ins_id)
		cur.execute(que1_1)
		ins_name = cur.fetchall()
		ins_name = ins_name[0][0].replace('&nbsp;','')
		ins_name = ins_name.strip()
		
	elif data2 and data2[0][1] > 40:
		ins_id = data2[0][0]
		que1_1 = "select name from tb_colleges_master_list where id='%d'"%(ins_id)
		cur.execute(que1_1)
		ins_name = cur.fetchall()
		ins_name = ins_name[0][0].strip()
		
	elif data3 and data3[0][1] > 40:
		ins_id = data3[0][0]
		que1_1 = "select name from tb_polytechnique_master_list where id='%d'"%(ins_id)
		cur.execute(que1_1)
		ins_name = cur.fetchall()
		ins_name = ins_name[0][0]
		ins_name = ins_name.strip()

	que_final = ""
	if ins_name=="none":
		que_final = "select id from institutes where name = '%s'" % (ins.replace("'","''"))
	else :
		que_final = "select id from institutes where name = '%s'" % (ins_name.strip())
	cur.execute(que_final)
	d = cur.fetchall()
	if d :
		print "matched :=>" + ins 
		return d[0][0]
	else :
		print "not matched :=>" + ins
		return -1
#institutes function final to be used
def write_degree_to_db(deg,deg_hec_score,abb,occ,ins,match_with_deg,con,cur,city,tbl_name,tbl2):
		id_of_row,row_data_occ = find_similar_row_in_db(deg,cur,tbl_name)
		lower_case = ins.lower()
		lower_case = lower_case.replace(",","")
		lower_case = lower_case.replace("'","''")
		ins = lower_case.replace("'","''")
		#institute_id = get_institute_id(ins,cur)
		que = ''
		if id_of_row == -1:
			deg_name = deg.replace("'","")
			hec_occ = deg_hec_score
			deg_abb = abb
			data_occ = occ
			que = "INSERT INTO %s " % (tbl_name)
			que = que + "(name,abb,mast_occ,data_occ,match_with) VALUES  ('%s','%s', '%d', '%d', '%d')" % (deg_name,abb,hec_occ,data_occ,match_with_deg)
			#print que
			cur.execute(que)
			#que2 = "select id from %s where name = '%s'" % (tbl_name,deg_name)
			#cur.execute(que2)
			#dd = cur.fetchall()
			#if dd:
			#	row_id = dd[0][0]
			#	que3 = "insert into %s (degree_id, institute_name,city,institute_id) values ('%d','%s','%s','%d')" %(tbl2,row_id,ins,city,institute_id)
			#	cur.execute(que3)
			#print "record added"
		else:
				fin_data_occ = row_data_occ + occ
				que = "UPDATE %s " % (tbl_name)
				que =  que + " SET data_occ = '%d' WHERE id='%d'" % (fin_data_occ,id_of_row) 
				#print que
				cur.execute(que) 
				#que3 = "insert into %s (degree_id, institute_name,city,institute_id) values ('%d','%s','%s','%d')" %(tbl2,id_of_row,ins,city,institute_id)
				#cur.execute(que3)
				#print "\t\t\tupdated"
		#con.commit()
	#rint "rows added :"+str(added)+" and rows updated : "+str(updated)
	#print "finalized data added in db"	
if __name__ == '__main__':
    app.run(host='0.0.0.0')
