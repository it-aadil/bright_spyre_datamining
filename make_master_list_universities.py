from flask import Flask, url_for, render_template, redirect , request,session
import json
import requests
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.security import Security, SQLAlchemyUserDatastore, UserMixin, RoleMixin
from authentication_model import db,User,Role
from flask_security import http_auth_required
from flask import jsonify
app = Flask(__name__)
import sys
from xml.dom.minidom import parse
import xml.dom.minidom
import json
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import operator
from operator import itemgetter
import psycopg2
import time
from make_master_list_colleges import make_confidential_score_c,reprocess_colleges
from make_master_list_polytechnique_institutes import make_confidential_score_p,reprocess_polytechnique
from make_degrees_master_list import make_confidential_score_degrees
import socket
from flask.ext.cors import CORS


timeout = 1000000
socket.setdefaulttimeout(timeout)
reload(sys)  
sys.setdefaultencoding('UTF')
CORS(app)


app.config['SECRET_KEY'] = 'super-secret'
app.config['DEFAULT_MAIL_SENDER'] = 'info@site.com'
app.config['SECURITY_REGISTERABLE'] = True
app.config['SECURITY_CONFIRMABLE'] = True
app.config['SECURITY_RECOVERABLE'] = True
#configuration for api's
app.config['SECURITY_PASSWORD_HASH']='pbkdf2_sha512'
app.config['SECURITY_TRACKABLE']='True'
app.config['SECURITY_PASSWORD_SALT']='2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824'
app.config['WTF_CSRF_ENABLED']='False'

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)

@app.route('/')
def index():
	response = Flask.make_response(app,render_template("data-entry.html"))
	response.headers["Access-Control-Allow-Origin"] = "*"
	return response
# Create a user to test with
@app.before_first_request
def create_user():
    db.create_all()
    if not User.query.first():
        user_datastore.create_user(email='aadil@cogilent.com', password='cogilent123')
        db.session.commit()
@app.route('/dummy-api/', methods=['GET','POST'])
@http_auth_required
def dummyAPI():
    r = {'status':'success'}
    return json.dumps(r)
@app.route('/circle-universities')
def function1():
	res = make_confidential_score()
	if res=="2":
		return redirect('/circle-universities')
		function1()
	else :
		return "Not enough more data to process"
@app.route('/circle-colleges')
def function2():
	res = make_confidential_score_c()
	if res =="2":
		return redirect('/circle-colleges')
		function2()
	else:
		return "Colleges Processed"
@app.route('/circle-polytechnique')
def function3():
	res = make_confidential_score_p()
	if res=="2":
		return redirect('/circle-polytechnique')
		function3()
	else:
		return "Polytechnique Institutes Processed"
@app.route('/manual-filter')
def function4():
	con , cur = connect_db()
	que1 = "select id,name,mast_occ,data_occ from tb_universities_score_list where mast_occ <= 40 and data_occ > 30 order by name"
	#que2 = "select * from tb_colleges_score_list where mast_occ <= 40 and data_occ > 30"
	#que3 = "select * from tb_polytechnique_score_list where mast_occ <= 40 and data_occ > 30"
	cur.execute(que1)
	tmp_arr = {}
	uni_names_only = ""
	to_del = []
	data1 = cur.fetchall()
	for row in data1:
		#print row[1]
		tmp_arr[row[0]] = {'id':row[0],'name':row[1],'mast_occ':row[2],'data_occ':row[3]}
		if row[1] and len(row[1])>1:
			uni_names_only= uni_names_only +"'"+ row[1] +"',"
	que2 = "select name from tb_colleges_score_list where mast_occ > 40 and name in (%s)" % (uni_names_only[:-1])
	cur.execute(que2)
	data2 = cur.fetchall()
	#del dictionary[_original_line]
	parameter_for_final_query = ""
	for row in data2:
		if row[0] and len(row[0]) > 1:
			to_del.append( row[0] )
			parameter_for_final_query = parameter_for_final_query + "'"+row[0] + "',"

	que3 = "select name from tb_polytechnique_score_list where mast_occ > 40 and name in (%s)" % (uni_names_only[:-1])
	cur.execute(que3)
	data3 = cur.fetchall()
	for row in data3:
		if row[0] and len(row[0]) > 1:
			to_del.append( row[0] )
			parameter_for_final_query = parameter_for_final_query + "'"+row[0] + "',"
	tmp = []
	print "lenfgth  of original array " + str(len(tmp_arr))
	final_dict = {}
	final_index = []
	que1 = "select id,name,mast_occ,data_occ from tb_universities_score_list where mast_occ <= 40 and data_occ > 40  and not name in(%s)  order by name" % (parameter_for_final_query[:-1])
	cur.execute(que1)
	data1 = cur.fetchall()
	for row in data1:
		final_dict[row[0]] = {'id':row[0],'name':row[1],'mast_occ':row[2],'data_occ':row[3]}
		#final_dict[row[0]] = {'name':row[1]}
		final_index.append(row[0])
	#final_index = sorted(final_index)
	return render_template("manual-filter.html",dictionary=final_dict,indexex=final_index)


	#	return """ 
	#<html> <head>
	#<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	#</head> """+ "<p>number of items found" + "<br><h1>Colleges</h1><br>"+str(data2).replace('(','<br>(')+"<br><h1>Polytechnque</h1><br>"+ str(data3).replace('(','<br>(')+"</p>"

	return """
	<html> <head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	 </head>
	 """ + "<p>"+str(final_dict).replace('{','{<br>')+"</p>"
@app.route('/reprocess-data-universities')
def function6():
	con , cur = connect_db()
	tbl_name = "tb_universities_score_list"
	tbl_name_master = "tb_universities_master_list"
	master_list = make_master_list(con,cur,tbl_name_master)
	limit = 500
	cur.execute("select location from tb_pointer where name = 'position1_reprocess' ")
	pointer_data = cur.fetchall()
	pointer = pointer_data[0][0]
	#que1 = "select id,name,mast_occ from %s where mast_occ <= 40 limit %d offset %d " % ( tbl_name , limit ,pointer)
	#que1 = "select id,name from %s  limit %d offset %d " % ( tbl_name , limit ,pointer)
	que1 = "select id,name from %s where mast_occ = 42 and match_with =12 " % (tbl_name)
	cur.execute(que1)
	data1 = cur.fetchall()
	if not len(data1) >= limit:
		return "No enough new data to process"
	count = 0
	print len(data1)
	for row in data1:
		count = count + 1
		matching, match_with = hec_confidential_score(row[1],master_list)
		if matching > 40:
			print "row number :\t"+str(count)
			que_ins = "update %s set match_with=%d,mast_occ=%d where id =%d" % ( tbl_name ,match_with , matching ,row[0])
			cur.execute(que_ins)
	qqq= "update tb_pointer set location=%d where name='position1_reprocess'" % (pointer+limit)
	cur.execute(qqq)
	con.commit()
	return "processed "
@app.route('/reprocess-data-colleges')
def function33():
	val = reprocess_colleges()
	if val=="0":
		return "Not enough new data to process"
	else :
		return redirect ('/reprocess-data-colleges')
@app.route('/reprocess-data-polytechnique')
def function44():
	val = reprocess_polytechnique()
	if val=="0":
		return "Not enough new data to process"
	else :
		return redirect ('/reprocess-data-polytechnique')
@app.route('/merge-all-institutes')
def function7():
	con , cur = connect_db()
	que1 = "select id,name,source,abb from tb_universities_master_list"
	que2 = "select id,name,source from tb_colleges_master_list"
	que3 = "select id,name,source from tb_polytechnique_master_list"
	cur.execute(que1)
	data1= cur.fetchall()
	cur.execute(que2)
	data2= cur.fetchall()
	cur.execute(que3)
	data3= cur.fetchall()
	abb=""
	data_occ=0
	mast_occ=50
	institute_type=""
	country=""
	city=""
	source=""
	#data uloading for universities
	for row in data1:
		que_tmp = "select sum(data_occ),max(mast_occ),max(city),max(country) from tb_universities_score_list where match_with= %d and mast_occ > 40 group by match_with" % (row[0])
		cur.execute(que_tmp)
		data_tmp = cur.fetchall()
		name = row [1].replace('&nbsp;',' ')
		name = name.strip()
		source =row[2]
		abb = row[3]
		institute_type = "university"
		if data_tmp:
			data_occ = data_tmp[0][0]
			mast_occ = data_tmp[0][1]
			city = data_tmp[0][2]
			country = data_tmp[0][3]
		que_chk = "select * from institutes where name='%s' and institute_type='university'" % (name)
		cur.execute(que_chk)
		data_chk = cur.fetchall()
		que_fin = ""
		if data_chk :
			que_fin = "update institutes set data_occ = '%d',country='%s',city='%s' where name='%s'" % (data_occ,country,city,name) 
		else:
			que_fin = "insert into institutes (name,abb,data_occ,mast_occ,institute_type,country,city,source) values ('%s','%s','%d','%d','%s','%s','%s','%s')" % (name,abb,data_occ,mast_occ,institute_type,country,city,source)
		print name +" : "+ str(data_occ) + " : " +str(mast_occ)+ " : " +city+ " : " +country
		cur.execute(que_fin)
	#data uloading for colleges
	for row in data2:
		que_tmp = "select sum(data_occ),max(mast_occ),max(city),max(country) from tb_colleges_score_list where match_with= %d and mast_occ > 40 group by match_with" % (row[0])
		cur.execute(que_tmp)
		data_tmp = cur.fetchall()
		name = row [1].replace('&nbsp;',' ')
		name = name.strip()
		source =row[2]
		abb = make_abbreviation_forstring(name)
		institute_type = "college"
		if data_tmp:
			data_occ = data_tmp[0][0]
			mast_occ = data_tmp[0][1]
			city = data_tmp[0][2]
			country = data_tmp[0][3]
		que_chk = "select * from institutes where name='%s' and institute_type='college'" % (name)
		cur.execute(que_chk)
		data_chk = cur.fetchall()
		que_fin = ""
		if data_chk :
			que_fin = "update institutes set data_occ = '%d',country='%s',city='%s' where name='%s'" % (data_occ,country,city,name) 
		else:
			que_fin = "insert into institutes (name,abb,data_occ,mast_occ,institute_type,country,city,source) values ('%s','%s','%d','%d','%s','%s','%s','%s')" % (name,abb,data_occ,mast_occ,institute_type,country,city,source)
		print name +" : "+ str(data_occ) + " : " +str(mast_occ)+ " : " +city+ " : " +country
		cur.execute(que_fin)
	#data uloading for polytechnique list
	for row in data3:
		que_tmp = "select sum(data_occ),max(mast_occ),max(city),max(country) from tb_polytechnique_score_list where match_with= %d and mast_occ > 40 group by match_with" % (row[0])
		cur.execute(que_tmp)
		data_tmp = cur.fetchall()
		name = row [1].replace('&nbsp;',' ')
		name = name.strip()
		source =row[2]
		abb = make_abbreviation_forstring(name)
		institute_type = "polytechnique"
		if data_tmp:
			data_occ = data_tmp[0][0]
			mast_occ = data_tmp[0][1]
			city = data_tmp[0][2]
			country = data_tmp[0][3]
		que_chk = "select * from institutes where name='%s' and institute_type='polytechnique'" % (name)
		cur.execute(que_chk)
		data_chk = cur.fetchall()
		que_fin = ""
		if data_chk :
			que_fin = "update institutes set data_occ = '%d',country='%s',city='%s' where name='%s'" % (data_occ,country,city,name) 
		else:
			que_fin = "insert into institutes (name,abb,data_occ,mast_occ,institute_type,country,city,source) values ('%s','%s','%d','%d','%s','%s','%s','%s')" % (name,abb,data_occ,mast_occ,institute_type,country,city,source)
		print name +" : "+ str(data_occ) + " : " +str(mast_occ)+ " : " +city+ " : " +country
		cur.execute(que_fin)

	con.commit()
	return " Request completed "
@app.route('/insert',methods=['GET'])
def function5():
	con , cur = connect_db()
	table = ""
	var = request.args['data']
	var_abb = make_abbreviation_forstring(var)
	cat = request.args['cat']
	if cat == "u":
		table = "tb_universities_master_list"
	elif cat == "c":
		table = "tb_colleges_master_list"
	elif cat == "p":
		table = "tb_polytechnique_master_list"
	if cat == "u":
		que = "insert into %s (name,abb) values ('%s','%s')" %(table,var,var_abb)
	else :
		que = "insert into %s (name) values ('%s')" %(table,var)
	print que
	cur.execute(que)
	con.commit()
	return que
@app.route('/add-degrees')
def function8():
	val = make_confidential_score_degrees()
	if val=="1":
		return "No more data to process"
	else :
		return redirect("/add-degrees")
	return "added"
@app.route('/companies/categories/suggestions',methods=['GET','POST'])
def function10():
	con , cur = connect_db()
	var = request.args['search']
	var = var.strip()
	data = ""
	if var:
		result = []
		var = var + "%"
		query = "select id,name from categories where name like '%s' " % (var)
		cur.execute(query)
		data = cur.fetchall()
		for row in data:
			result.append({'id':row[0],'name':row[1]})
	return json.dumps(result)
@app.route('/companies/subcategories/suggestions',methods=['GET','POST'])
def function11():
	con , cur = connect_db()
	var1 = request.args['maincategory']
	var1= var1.strip()
	var2 = request.args['subcategory']
	var2= var2.strip()
	data = ""
	if var1 and var2:
		result = []
		var2 = var2 + "%"
		query ="select categories.name,subcategories.name from categories inner join subcategories on categories.id=subcategories.category_id where categories.name ='%s' and subcategories.name like '%s';" % (var1,var2)
		cur.execute(query)
		data = cur.fetchall()
		for row in data:
			result.append({'category':row[0],'subcategory':row[1]})
	return json.dumps(result)
@app.route('/companies/detail/add',methods=['GET','POST'])
def function12():
	con , cur = connect_db()
	_id = request.form['id']
	_id = int(_id)
	maincategory = request.form['maincategory'].strip()
	subcategory = request.form['subcategory'].strip()
	website = request.form['website'].strip()
	description = request.form['description'].strip()
	hiring_description = request.form['how_they_hire'].strip()
	
	startups = request.form['startups'].strip()
	hiring_now = request.form['hiring_now'].strip()
	hire_internees = request.form['hire_internees'].strip()
	hire_mtos = request.form['hire_management_trainees'].strip()

	maincategory = maincategory.lower()
	subcategory = subcategory.lower()
	website = website.lower()
	description = description.lower()
	hiring_description = hiring_description.lower()


	subcategory_arr = subcategory.split(',')
	subcat="{"
	if subcategory:
		for val in subcategory_arr:
			subcat = subcat + val +","
		subcat = subcat[:-1] + "}"
	else:
		subcat = "{}"
	query = "update companies set maincategory='%s',subcategory='%s',website='%s',description='%s', how_they_hire='%s',hiring_now ='%s',hire_internees='%s',hire_management_trainees='%s', startups='%s' where id = %d " % (maincategory,subcat,website,description,hiring_description,hiring_now,hire_internees,hire_mtos,startups,_id)
	cur.execute(query)
	con.commit()
	result = []
	result.append({'result':'Successfull'})
	return json.dumps(result)
def function1():
	res = make_confidential_score()
	if res=="2":
		return redirect('/circle-universities')
		function1()
	else :
		return "Not enough more data to process"
def ordinary_word(word):
	or_words = []
	or_words.append('the')
	or_words.append('of')
	#or_words.append('the')
	if not word in or_words:
		return 1
	else:
		return 0
def already_added(lower_case):
	if lower_case in saved_names:
		return 1
	else:
		return 2
def convert_to_string(words_array):
	tmp_line = ''
	if len(words_array)>0:
		for word in words_array:
			tmp_line = tmp_line +' '+ word
	return tmp_line
def make_abbreviation(lower_case):
	#return 'working on it'
	abb = ''
	for word in lower_case:
		if ordinary_word(word) == 1:
			abb = abb + word[:1]
	return abb
def exists(sou,des):
		qwerty_count = 0
		t_matching = 0
		t_name = ''
		seq_num = 0
		t_rep = 0
		for lll in des:
			if lll and des[lll]['1'] == sou :
				#t_matching = fuzz.token_set_ratio( des[lll]['1'] , sou )
				t_rep = des[lll]['4']
				t_name = des[lll]['1']
				seq_num = qwerty_count
				return des[lll]['4'], qwerty_count, 'none'
			qwerty_count= qwerty_count + 1	
		return -1,-1 , ''
def split_in_columns(line):
	return line.split('"')
	tt_array = []
	items = 1
	if line:
		count3 = -1
		count = 0
			#for ch in line:
			#logic for old file , with double cots along all parameters
			# if ch=='"' and count > count3:
			# 	start = count + 1
			# 	count2 = 0
			# 	#count = count + 1
			# 	for ch2 in line:
			# 		if count2 > count:
			# 		 	if ch2 == '"':
			# 		 		end = count2 - 1
			# 		 		if not start > end:
			# 		 			tt_array.append(line[start:end+1])
			# 		 	 	else:
			# 		 	 		tt_array.append('')
			# 		 	 	items = items + 1
			# 		 	 	count3 = count2
			# 		 	 	break
			# 		count2 = count2 + 1
			# count = count + 1

	return tt_array
def make_abbreviation_forstring(txt):
	str_arr = txt.replace('and','')
	str_arr = str_arr.replace('&','')
	str_arr = str_arr.replace('of','')
	str_arr = str_arr.replace('the','')
	str_arr = str_arr.replace(',','')
	str_arr = str_arr.split(' ')
	abb_tmp = ''
	for word in str_arr:
		abb_tmp = abb_tmp+word[:1]
	return abb_tmp
def make_cities_list(con,cur):
	cur.execute("select * from tb_cities_master_list")
	cities = []
	lines = cur.fetchall()
	for city_f in lines:
		city  = city_f[1]
		txt_t = city.lstrip()
		txt_t = txt_t.lower()
		cities.append(txt_t)
	return cities	
@app.route('/make-master-list')
def make_master_list(con,cur,tbl_name):
	dummy = {}
	query = "select id,name from %s" % (tbl_name)
	cur.execute(query) # tb_polytechnique_institutes_list
	lines = cur.fetchall()
	counter = 0
	limit = 3000
	for line in lines:
		abb = ''
		counter= counter + 1
		institute = line[1]
		institute = institute.replace("&nbsp;"," ")
		institute = institute.replace(",","")
		institute = institute.lower()
		abb = make_abbreviation_forstring(institute)
		dummy[institute]={'id':line[0],'name':institute,'abb':abb}	        				
	return dummy
def remove_if_dual_city(tmp_city,city):
	#print tmp_city
	tmp_index = tmp_city.find(city)
	if tmp_index > -1:
		v_val_1 = tmp_city[0:tmp_index+len(city)]
		v_val_2 = tmp_city[tmp_index+len(city):len(tmp_city)]
		v_val_2 = v_val_2.replace(v_val_2,'')
		tmp_city = v_val_1+v_val_2
	#print tmp_city
	return tmp_city
	#t1[12:len(t1)]
def remove_city_names(_original_line,cities_list):
	#print len(cities_list)
	tmp_city = _original_line
	for city in cities_list:
		city = city.strip()
		if len(city) > 2:
			conc_of = "of "+city
			conc_of_2 = "the "+city
			if conc_of in _original_line or conc_of_2 in _original_line:
				co = 1 +1
				tmp_city = remove_if_dual_city(tmp_city,city)
			else :
				#print city +" <in>" + _original_line
				if city in _original_line:
					#print "city matched"
					tmp_city = tmp_city.replace(city,'')
					#print tmp_city +">"+ _original_line+">"+city
					#print city
				
	#print tmp_city

	return tmp_city
@app.route('/check-db')
def connect_db():
	conn_string = "host='ec2-54-225-120-137.compute-1.amazonaws.com' dbname='d50o3fq3c5v493' user='ofpptufxpgbgor' password='jUeHlLmegfrgKVDns9v5UNEc-T'"
	conn = psycopg2.connect(conn_string)
	cursor = conn.cursor()
	#ddata= cursor.execute("create table table1(name char(40));")
	#rty = cursor.execute("insert into table1 values('testing second try')")
	#conn.commit()
	return conn,cursor
def is_not_college_or_school(degree,list_arr):
	#return True
	degree = degree.replace('.','')
	degree = degree.replace(' ','')
	degree = degree.lower()
	for val in list_arr:
		if val in degree:
			return False
	return True
@app.route('/read-data')
def read_data(con,cur):
		cur.execute("select * from tb_pointer where name = 'position1'")
		qwe = cur.fetchall()
		start=  qwe[0][1]
		#start = 50000	
		#start= 55000 #to remove
		increment = 100
		end = start + increment
		q1 = "SELECT education_id,title,institute,country,city  FROM education WHERE education_id >= '%d' AND education_id < '%d' " %(start,end)
		#active belowe line
		q2 = "UPDATE tb_pointer SET location = '%d' WHERE name='position1'" % (end) #dfdf
		print "data read range :"+str(start)+" --to-- "+str(end)
		#print q1
		#print q2
		cur.execute(q2) 
		cur.execute(q1) 
		#con.commit()
		lines = cur.fetchall()
		d_array = []
		saved_names = []
		dictionary = {}
		if not lines or len(lines) < increment:
			return dictionary
		#counter = 0
		col_schl = ["matric","metric","ssc","matriculation","hssc","fsc","olevel","alevel","dae"]
		for line in lines:
			sss = line[2]
			sss= sss.decode('ascii','ignore').strip()
			#print  sss
			#counter = counter + 1
			# if counter > 3000:
			#  	break
			if is_not_college_or_school(line[1],col_schl):
				if sss and len(sss) > 1:
					dat = sss
					if dat and len(dat) > 1 :
						lower_case = sss.lower()
	    				lower_case = lower_case.replace(',','')
	    				lower_case = lower_case.replace("'","''")
	    				_original_line = lower_case #added original line
	    				abb = make_abbreviation_forstring(lower_case)
	    				_abbreviation = abb #abbreviation added
	    				#print _abbreviation
	    				_country = line[3]
	    				_repitition = 1
	    				_city = line[4]
	    				(noc,dd,to_del) = exists(_original_line,dictionary)
	    				if noc == -1:
	    					_repitition = 1
	    					#print "No repeat"
	    				else:
	    					
	    					_repitition = noc + 1
	    					#print "repeated ="+ str(_repitition)
	    					#print _original_line + str(noc)
	    					del dictionary[_original_line]
	    				dictionary[_original_line] = {'1':_original_line,'2':'none','3':_abbreviation,'4':_repitition,'5':_country,'6':_city}
	    	#else:

   		#print "reading done"
		return dictionary
def contains_city(tmp_name,cities_list):
	for city in cities_list:
		if city and city in tmp_name:
			return 0
	return 1
def hec_confidential_score(obj2,hec_list):
	tmp_name = ''
	tmp_matching = 0
	abb_name = ''
	abb_matching = 0
	match_for_abb = obj2
	tmp_name_id = -1
	tmp_abb_id = -2
	for ooo in hec_list:
		#print ooo
		ooo_tmp = ooo.replace('&nbsp;',' ')
		ooo_tmp = ooo_tmp.replace('&','')
		ooo_tmp = ooo_tmp.replace('and','&')
		ooo_tmp = ooo_tmp.replace('-','')
		ooo_tmp = ooo_tmp.replace('.','')

		#obj2_abb = make_abbreviation(obj2)
		
		ratio_ratio = fuzz.token_sort_ratio(obj2, ooo_tmp)
		if ratio_ratio > tmp_matching:
			tmp_matching = ratio_ratio
			tmp_name = obj2
			#print str(fuzz.token_set_ratio(obj2, ooo_tmp)) +":"+ str(tmp_matching)
			tmp_name_id = hec_list[ooo]['id']
			#print obj2 + "\t:" + ooo_tmp +"\t id is "+str(tmp_name_id) +"\t per" + str(tmp_matching)
		r_t = hec_list[ooo]['abb']
		ratio_abb = fuzz.ratio(match_for_abb,r_t)
		if ratio_abb > abb_matching:
			abb_matching = ratio_abb
			abb_name = hec_list[ooo]['name']
			tmp_abb_id =hec_list[ooo]['id']
	#print obj2+str(tmp_name_id)+"abb"+str(tmp_abb_id)
	if tmp_matching >60:
		return tmp_matching/2,tmp_name_id
	else :
		if abb_matching > 80:
			return abb_matching/2,tmp_abb_id
		else :
			return tmp_matching/2,tmp_name_id
def data_confidential_score(obj3):
	#if(obj3 > 50 ):
	#	return 50
	#else:
	return obj3
def find_similar_row_in_db(ins_name,cur,tbl_name):
	ins_name = ins_name.replace("'","''")
	que = "select * from %s where name = '%s'" % (tbl_name,ins_name)
	#if "peshawar" in ins_name:
	#	print que
	#print ins_name
	cur.execute(que)
	tmp_data = cur.fetchall()
	inst_occ = 0
	inst_id =0
	chk = 0
	for ww in tmp_data:
		chk = chk + 1
		inst_occ = ww[3]
		inst_id = ww[7]
		#print ins_name+ ":" + str(inst_occ)
		#print ww[1]
		return inst_id,inst_occ,
	return -1,-1
@app.route('/get-colleges-from-filtered-list/<search_kind>')
def get_colleges_list_from_filtered_data(search_kind=None):
	con,cur= connect_db()
	val = '%s'%(search_kind)
	val = '%'+val+'%'
	que = ''
	if search_kind == "allcolleges":
		que = "select * from tb_filtered_list WHERE name LIKE '%post graduate%' OR name LIKE '%postgraduate%' OR name LIKE '%degree college%' OR name LIKE '%degreecollege' OR name LIKE '%model college%' OR name LIKE '%modal college%' OR name LIKE '%college of%' AND name NOT LIKE '%university%' ORDER BY (data_occ + hec_occ) DESC"
	else:
		que = "select * from tb_filtered_list WHERE name LIKE '%s' ORDER BY (data_occ + hec_occ) DESC " %(val)
	#print que
	cur.execute(que)

	data = cur.fetchall()
	return render_template('display-text.html',data= data)
def write_result_to_db(fin_dict,con,cur):
	#print "Length of data uploaded"+str(len(fin_dict))
	#cur.execute("delete from tb_filtered_list")
	#cur.execute("create table tb_filtered_list(id int , name char(400), hec_occ int, data_occ int, category char(100), PRIMARY KEY (id));")
	tbl = 'tb_filtered_list'
	query2 = "SELECT MAX(id) As hv FROM %s;" % (tbl)
	cur.execute(query2)
	max_v = cur.fetchall()
	counter = 0
	if max_v[0][0]:
		counter = max_v[0][0]
	for data in fin_dict:
		counter = counter + 1
		#print fin_dict[data]['name']+":"+str(fin_dict[data]['hec-occurence'])+":"+str(fin_dict[data]['data-occurence'])+":"+fin_dict[data]['category']
		id_of_row,row_data_occ = find_similar_row_in_db(fin_dict[data]['name'],cur,tbl)
		que = ''
		if id_of_row == -1:
			#print "new rec added"
			ccc = fin_dict[data]['hec-occurence'] + fin_dict[data]['data-occurence']
			catt= ''
				#if fin_dict[data]['hec-occurence'] > 30 and fin_dict[data]['data-occurence'] >30:
				#	catt = "university"
				#else :
				#	catt= "unknown institution = insert"
			que = "INSERT INTO tb_filtered_list VALUES  ('%d','%s', '%d', '%d', '%s' )" % (counter,fin_dict[data]['name'], fin_dict[data]['hec-occurence'], fin_dict[data]['data-occurence'], fin_dict[data]['category'])
			cur.execute(que)
			#print "1 =:>" + que 
			#if "peshawar" in fin_dict[data]['name']:
			#	print que
		else:
			#print "wrong conditino called"+fin_dict[data]['name']
			fin_data_occ = row_data_occ + fin_dict[data]['data-occurence']
			#print str(row_data_occ) +":"+str(fin_dict[data]['data-occurence'])
			#fin_data_occ = fin_data_occ % 50
			if fin_data_occ > 50:
				#print "max limit achieved"
				#print fin_dict[data]['name'] + ":"+cur + ":" +str(fin_data_occ)
				fin_data_occ = 50
			#print fin_dict[data]['data-occurence']
			#print row_data_occ
			#fin_data_occ = fin_data_occ % 50
			#print fin_dict[data]['name']
			ffff= fin_dict[data]['data-occurence'] + row_data_occ
			#print ffff
			#print id_of_row
			#fin_data_occ = fin_data_occ % 50
			cat= 'unknown institution2'
				#if fin_dict[data]['data-occurence'] > 30 and row_data_occ >30:
				#	cat = "university"
				#else:
				#	cat ="unknown institution =update"
			que = "UPDATE tb_filtered_list SET data_occ = '%d' WHERE id='%d'" % (fin_data_occ,id_of_row) 
			#print "2 =:>" + que 
			cur.execute(que)
			#print que
			#print "update query successfull"
	con.commit()
	print "finalized data added in db"	
@app.route('/make-cofidential-score')
def make_confidential_score():
	con,cur = connect_db()
	fin_dict = {}
	count = 0
	print "Creating Master List Universities"
	hec_list = make_master_list(con,cur,'tb_universities_master_list')#last column is name of table from which master list is to be created
	#print "Now reading data from Server"
	file_data = read_data(con,cur)
	if not file_data:
		return "1"
	print "Now Calculating Confidence Score"
	cities_list = make_cities_list(con,cur)
	count = 0 
	for obj in file_data:
			count = count + 1
			#print count
		 	obj2_tmp2 = obj.replace('&','')
		 	obj2_tmp2 = obj2_tmp2.replace('and','')
		 	obj2_tmp3 = remove_city_names(obj2_tmp2,cities_list)
		 	hec_conf, match_with_id = hec_confidential_score(obj2_tmp3,hec_list) # confidential score calculated from hec master list 
		   	#print "caclutaing data confindential score"
		   	occ_conf = data_confidential_score(file_data[obj]['4']) #cs from file data
		   	obj2_tmp2 = obj2_tmp2.replace('^','')
		   	ins_type = ''
		   	sum_conf_scr = 0
		   	sum_conf_scr = hec_conf + occ_conf
		   	ins_type = file_data[obj]['5']
		   	city = file_data[obj]['6']
		   	abb = file_data[obj]['3']
		   	fin_dict[obj2_tmp2] = {'name':obj2_tmp3,'abb':abb,'hec-occurence':hec_conf, 'data-occurence':occ_conf,'category':'university', 'country':ins_type,'city':city,'match_with' : match_with_id }
		   	#print "line processed"+str(count)
	print "Now Uploading data to server"	 
	write_institutes_to_db(fin_dict,con,cur,'tb_universities_score_list')
	#print "\t\tdata inserted"
	#return "data added"
	return "2"
	return redirect('/add-data-continuously')
@app.route('/add-data-continuously')
def start_loop():
	time.sleep(3)
	return redirect("/make-cofidential-score")
	while True:
		x = make_confidential_score()
		if x == 1:
			break
	return "<h1 style='color:blue'>All data uploaded to Server! Successfull</h1>"
@app.route('/save-master-list-in-db')
def add_master_list_to_db():
	conn,cur = connect_db()
	count = 0
	raw_dict = read_data(con,cur)
	#cur.execute("select * from tb_raw_data")
	#for xx in master_list:
	#	count = count +1
	#	#que = "insert into tb_master_list values ("+str(count)+",'"+master_list[xx]['name']+"','"+master_list[xx]['abb']+"')"
	#	que = "INSERT INTO tb_cities_list VALUES ('%d', '%s' )" % (count, xx)
	#	cur.execute(que)
	#conn.commit()
	#result = cur.fetchall()
	#return 'data added'
	#for line in raw_dict :
		#que = ""
		#cur.execute(que)
	return render_template('display-text.html',data = result)
@app.route('/update-parent')
def up_date():
	con,cur = connect_db()
	mas_list = make_master_list(con,cur,"tb_universities_master_list")
	cur.execute("select id,name from tb_universities_score_list where match_with = -1")
	data = cur.fetchall()
	cities_list = make_cities_list(con,cur)
	for val in data:
		name = val[1]
		name = remove_city_names(name,cities_list)
		hec_conf, match_with_id = hec_confidential_score(name,mas_list)
		#print name + str(len(mas_list))
		que = "update tb_universities_score_list set match_with = '%d' where id ='%d'" % (match_with_id,val[0])
		cur.execute(que)
		#print val[1]+":\tmatched with\t:"+str(match_with_id)
	con.commit()
	return "updae done"
@app.route('/view-data')
def view_data():
	conn,cur = connect_db()
	q = "select * from tb_universities_score_list where mast_occ >35 AND data_occ > 10  ORDER BY (data_occ + mast_occ) DESC "
	cur.execute(q)
	#print q
	#cur.execute("select * from tb_raw_data")
	da = cur.fetchall()
	#conn.commit()
	return render_template('display-text.html', data= da)
# colleges function is old not modified
def write_colleges_to_db(fin_dict,con,cur):
	#print "Length of data uploaded"+str(len(fin_dict))
	#cur.execute("delete from tb_colleges_score_list")
	#cur.execute("create table tb_colleges_score_list(id int , name char(400), mas_occ int, data_occ int, category char(100), PRIMARY KEY (id));")
	tbl = 'tb_colleges_score_list'
	query3 = "SELECT MAX(id) As hv FROM %s;" % (tbl) 
	cur.execute()
	max_v = cur.fetchall()
	counter = 0
	if max_v[0][0]:
		counter = max_v[0][0]
	for data in fin_dict:
		counter = counter + 1
		#print fin_dict[data]['name']+":"+str(fin_dict[data]['hec-occurence'])+":"+str(fin_dict[data]['data-occurence'])+":"+fin_dict[data]['category']
		id_of_row,row_data_occ = find_similar_row_in_db(fin_dict[data]['name'],cur,tbl)
		que = ''
		if id_of_row == -1:
			#print "new rec added"
			ccc = fin_dict[data]['hec-occurence'] + fin_dict[data]['data-occurence']
			catt= ''
				#if fin_dict[data]['hec-occurence'] > 30 and fin_dict[data]['data-occurence'] >30:
				#	catt = "university"
				#else :
				#	catt= "unknown institution = insert"
			que = "INSERT INTO %s VALUES " % (tbl)	
			que =   que + "('%d','%s', '%d', '%d', '%s' )" % (counter,fin_dict[data]['name'], fin_dict[data]['hec-occurence'], fin_dict[data]['data-occurence'], fin_dict[data]['category'])
			cur.execute(que)
			#print "1 =:>" + que 
			#if "peshawar" in fin_dict[data]['name']:
			#	print que
		else:
			#print "wrong conditino called"+fin_dict[data]['name']
			fin_data_occ = row_data_occ + fin_dict[data]['data-occurence']
			#print str(row_data_occ) +":"+str(fin_dict[data]['data-occurence'])
			#fin_data_occ = fin_data_occ % 50
			if fin_data_occ > 50:
				#print "max limit achieved"
				#print fin_dict[data]['name'] + ":"+cur + ":" +str(fin_data_occ)
				fin_data_occ = 50
			#print fin_dict[data]['data-occurence']
			#print row_data_occ
			#fin_data_occ = fin_data_occ % 50
			#print fin_dict[data]['name']
			ffff= fin_dict[data]['data-occurence'] + row_data_occ
			#print ffff
			#print id_of_row
			#fin_data_occ = fin_data_occ % 50
			cat= 'unknown institution2'
				#if fin_dict[data]['data-occurence'] > 30 and row_data_occ >30:
				#	cat = "university"
				#else:
				#	cat ="unknown institution =update"
			que = "UPDATE %s" % ( tbl )	
			que =  que + "SET data_occ = '%d' WHERE id='%d'" % (fin_data_occ,id_of_row) 
			#print "2 =:>" + que 
			cur.execute(que)
			#print que
			#print "update query successfull"
	con.commit()
	print "finalized data added in db"	
#institutes function final to be used
def write_institutes_to_db(fin_dict,con,cur,tbl_name):
	#print "length of data to be uploaded :" + str(len(fin_dict))
	added = 0
	updated = 0
	for data in fin_dict:
		#counter = counter + 1
		#print fin_dict[data]['name']+":"+str(fin_dict[data]['hec-occurence'])+":"+str(fin_dict[data]['data-occurence'])+":"+fin_dict[data]['category']
		id_of_row,row_data_occ = find_similar_row_in_db(fin_dict[data]['name'],cur,tbl_name)
		que = ''
		if id_of_row == -1:
			added = added + 1
			#print "new rec added"
			ccc = fin_dict[data]['hec-occurence'] + fin_dict[data]['data-occurence']
			catt= ''
			name_ins = fin_dict[data]['name'].strip()
			name_ins = name_ins.replace("'","''")
			city_add = fin_dict[data]['city']
			city_add = city_add.replace("'","")
			que = "INSERT INTO %s " % (tbl_name)
			abb = fin_dict[data]['abb']
			abb = abb.replace("'","")
			que = que + "(name,abb,mast_occ,data_occ,category,country,city,match_with) VALUES  ('%s','%s', '%d', '%d', '%s','%s','%s','%d' )" % (name_ins,abb, fin_dict[data]['hec-occurence'], fin_dict[data]['data-occurence'],fin_dict[data]['category'], fin_dict[data]['country'].replace("'",""),city_add,fin_dict[data]['match_with'])
			#print que
			cur.execute(que)

		else:
			if True :
				updated = updated + 1
				#print "record updated"
				fin_data_occ = row_data_occ + fin_dict[data]['data-occurence']
				if fin_data_occ > 50:
					fin_data_occ = 50
				ffff= fin_dict[data]['data-occurence'] + row_data_occ
				cat= 'unknown institution2'
				que = "UPDATE %s " % (tbl_name)
				que =  que + " SET data_occ = '%d' WHERE id='%d'" % (fin_data_occ,id_of_row) 
				cur.execute(que) #.execution_options(autocommit=False)
	con.commit()
	print "rows added :"+str(added)+" and rows updated : "+str(updated)
	#print "finalized data added in db"	
if __name__ == '__main__':
    app.run(host='0.0.0.0')
