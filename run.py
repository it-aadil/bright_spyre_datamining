from flask import Flask, url_for, render_template, request, redirect
app = Flask(__name__)
import sys
reload(sys)  
sys.setdefaultencoding('Cp1252')

@app.route('/')
def index():
	return render_template('for_signup.html')
def post(self):
	return "data posted"
@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
	return render_template('hello.html', name=name)
@app.route('/user/<username>')
def show_user_profile(username):
	return 'User %s' % username
@app.route('/post/<int:post_id>')
def show_post(post_id):
	return 'Post %d' % post_id
@app.route('/sss',methods=['POST'])
def create_account():
	return request.form['fname']	
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        do_the_login()
    else:
        show_the_login_form()
def show_the_login_form():
	return render_template('login.html',post_on='/verify_login')
def already_added(lower_case):
	if lower_case in saved_names:
		return 1
	else:
		return 2
def convert_to_string(words_array):
	tmp_line = ''
	if len(words_array)>0:
		for word in words_array:
			tmp_line = tmp_line +' '+ word
	return tmp_line
def split_in_columns(line):
	tt_array = []
	items = 1
	if line:
		count3 = -1
		count = 0
		for ch in line:
			if ch=='"' and count > count3:
				start = count + 1
				count2 = 0
				#count = count + 1
				for ch2 in line:
					if count2 > count:
					 	if ch2 == '"':
					 		end = count2 - 1
					 		if not start > end:
					 			tt_array.append(line[start:end+1])
					 	 	else:
					 	 		tt_array.append('')
					 	 	items = items + 1
					 	 	count3 = count2
					 	 	break
					count2 = count2 + 1
			count = count + 1		 	 	
	return tt_array
def ordinary_word(word):
	or_words = []
	or_words.append('the')
	or_words.append('of')
	#or_words.append('the')
	if not word in or_words:
		return 1
	else:
		return 0
def make_abbreviation(lower_case):
	#return 'working on it'
	abb = ''
	for word in lower_case:
		if ordinary_word(word) == 1:
			abb = abb + word[:1]
	return abb
def check_prequisites(val) :
	if(val.isdigit()) :
		return 0
	else :
		return 1
def no_of_occurence(sour,dest):
	for rr in dest:
		 	if(rr[2]==sour):
		 		return rr[2] + 1
	return 1
def exists(sou,des):
		qwerty_count = 0
		for lll in des:
			if lll and lll[1] == sou:
				return lll[3],qwerty_count
			qwerty_count= qwerty_count + 1	
		return -1,-1
@app.route('/read-file')
def login_mail():
	#return 'aadil'
    with open('institute_names.txt') as f:
    	content = f.read()
    	d_array = []
    	saved_names = []
    	#vvv = "THIS IS TESTING STRING"
    	#d_array.append(vvv.lower())
    	#d_array.append('my array')
    	lines = content.split('\n')
    	chk = 0
    	for line in lines:
    		chk = chk + 1
    		ins_name = split_in_columns(line)
    		if len(ins_name) > 1:
    			tmp_arr = []
    			#d_array.append(ins_name[1])
    			dat = ins_name[1]
    			if len(dat) > 1 and check_prequisites(dat) == 1:
    				lower_case = ins_name[1].lower()
    				words_array = lower_case.split(' ')
    				_original_line = lower_case #added original line
    				abb = make_abbreviation(words_array)
    				words_array.sort()
    				sorted_string = convert_to_string(words_array)
    				_sorted_line = sorted_string # sorted string added
    				_abbreviation = abb #abbreviation added
    				
    				_repitition = 1
    				(noc,dd) = exists(sorted_string,d_array)
    				if noc == -1:
    					_repitition = 1
    					tmp = []
	    				tmp.append(_original_line)
	    				tmp.append(_sorted_line)
	    				tmp.append(_abbreviation)
	    				tmp.append(_repitition)
	    				d_array.append(tmp)
    				else:
    					_repitition = noc + 1
    					d_array[dd][3] = noc + 1
    				
    		
    	d_array.append(len(d_array))		
    	return render_template('display-text.html',data= d_array)

if __name__ == '__main__':
    app.run(host='0.0.0.0')
