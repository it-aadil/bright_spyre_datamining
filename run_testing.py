from flask import Flask, url_for, render_template
app = Flask(__name__)
import sys
import sqlalchemy
from sqlalchemy import Table, Column, Integer, String, ForeignKey
from dbmodel import db,Usser
import psycopg2
reload(sys)  
sys.setdefaultencoding('Cp1252')

@app.route('/')
def index():
	return 'Home Page'

@app.route('/user/<username>')
def show_user_profile(username):
	return 'User %s' % username
@app.route('/post/<int:post_id>')
def show_post(post_id):
	return 'Post %d' % post_id
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        do_the_login()
    else:
        show_the_login_form()
@app.route('/models_testing')
def models_testing_phase():
	db.create_all()
	admin = Usser('aadil1', 'aadil@cogilent.com1')
	user = Usser('user1', 'symfony@php.com1')
	db.session.add(admin)
	db.session.add(user)
	db.session.commit()
	return 'model created'
@app.route('/sqlalchemy-test')
def sqlalchemy_test():
	con, meta = connect('postgres', 'cogilent123', 'db_python')
	#create tables syntax below 5 lines
	#slams = Table('slams', meta,
    #Column('name', String, primary_key=True),
    #Column('country', String)
	#)
	#meta.create_all(con)
	table = meta.tables['slams']
	clause = table.insert().values(name='Symfony', country='Pakistan')
	result = con.execute(table.select().where(table.c.name == 'Aadil'))
	for row in result:
		print row[0]
	#print result.inserted_primary_key
	return 'data inserted in table'

def connect_db():
	conn_string = "host='localhost' dbname='db_python' user='postgres' password='cogilent123'"
	conn = psycopg2.connect(conn_string)
	cursor = conn.cursor()
	return conn,cursor
def connect_db_2():
	conn_string = "host='ec2-54-225-120-137.compute-1.amazonaws.com' dbname='d50o3fq3c5v493' user='ofpptufxpgbgor' password='jUeHlLmegfrgKVDns9v5UNEc-T'"
	conn = psycopg2.connect(conn_string)
	cursor = conn.cursor()
	return conn,cursor

def connect(user, password, db, host='localhost', port=5432):
    '''Returns a connection and a metadata object'''
    # We connect with the help of the PostgreSQL URL
    # postgresql://federer:grandestslam@localhost:5432/tennis
    url = 'postgresql://{}:{}@{}:{}/{}'
    url = url.format(user, password, host, port, db)
    # The return value of create_engine() is our connection object
    con = sqlalchemy.create_engine(url, client_encoding='utf8')
    # We then bind the connection to MetaData()
    meta = sqlalchemy.MetaData(bind=con, reflect=True)
    return con, meta
def fetch_data(cur,name):
	q = 'select name from %s' % (name)
	cur.execute(q)
	return cur.fetchall()
@app.route('/add-table-online')
def function():
	con,cur = connect_db()
	data_list = fetch_data(cur,'tb_polytechnique_institutes_list')
	con.close()
	con, cur = connect_db_2()
	#count =0
	for row in data_list:
		#count = count + 1
		val = row[0]
		val = val.lower()
		que = "insert into tb_polytechnique_master_list (name) values ('%s')" % (val)
		cur.execute(que)
	con.commit()
	return "data added"

if __name__ == '__main__':
    app.run(host='0.0.0.0')
